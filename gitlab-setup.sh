#!/bin/bash

# Resources
# https://docs.gitlab.com/charts/development/minikube/

# VARIABLES
HOST=127.0.0.1  # minikube with docker driver (use minikube tunnel)

# FUNCTIONS
function kubeCheck() {
    . ./startup.sh
}

function installGitlab() {
    echo -e "\n*** Installing GitLab ***"
    helm repo add gitlab https://charts.gitlab.io/
    helm repo update
    helm upgrade --install gitlab gitlab/gitlab \
        --timeout 600s   \
        --set global.hosts.domain=127.0.0.1.nip.io \
        --set global.hosts.externalIP=127.0.0.1 \
        -f https://gitlab.com/gitlab-org/charts/gitlab/raw/master/examples/values-minikube-minimum.yaml
}

function finalSteps() {
    echo -e "\n*** Final Steps ***"
    kubectl get secret gitlab-wildcard-tls-ca -ojsonpath='{.data.cfssl_ca}' | base64 --decode > gitlab.127.0.0.1.nip.io.ca.pem
    echo -e "Access your GitLab instance here: https://gitlab.127.0.0.1.nip.io/users/sign_in"
    PASSWORD=$(kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 -d)
    echo -e "User: root \nPassword: ${PASSWORD}"
}

# STEPS
kubeCheck
installGitlab
finalSteps

exit 0
