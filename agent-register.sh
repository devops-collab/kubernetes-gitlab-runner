#!/bin/bash

# VARIABLES
TOKEN=""
GITLAB_AGENT=gitlab-agent-$(echo $1 | cut -c 1-13 | tr '[:upper:]' '[:lower:]' | sed "s/[^[:alnum:]-]//g")
TAG=v15.8.0  
# latest stable version found with command:
# helm search repo gitlab | grep "gitlab/gitlab-agent" | cut -d "v" -f 2 | cut -d " " -f 1

# ARGS
if [[ $1 != "" ]]
then
    TOKEN=$1
fi

# FUNCTIONS
function checkToken() {
    echo -e "\n*** Token Check ***"
    if [[ $TOKEN == "" ]]
    then 
        echo -e "\nPlease re-run the script passing the GitLab Agent Token as argument "
        exit 1
    else
        echo "ok"
    fi
}

function kubeCheck() {
    . ./startup.sh
}

function prepareDescriptors() {
    echo -e "\n*** Preparing Descriptors ***"
    read -p "Please enter a name for your GitLab Agent (default: gitlab-agent-<token>) " NAME
    GITLAB_AGENT=${NAME:=$GITLAB_AGENT}
    echo -e \
    "TOKEN \t \t: ${TOKEN} \nGITLAB_AGENT \t: ${GITLAB_AGENT} "
    cp values-template.yaml values.yaml
    sed -i "s|\${TOKEN}|$TOKEN|g" values.yaml
}

function applyDescriptors() {
    echo -e "\n*** Applying Descriptors ***"
    helm repo add gitlab https://charts.gitlab.io
    helm repo update
    helm upgrade --install $GITLAB_AGENT gitlab/gitlab-agent \
        --namespace gitlab-agent \
        --create-namespace \
        --set image.tag=$TAG \
        --set config.token=$TOKEN \
        --set config.kasAddress=wss://kas.gitlab.com
}

# STEPS
checkToken
kubeCheck
prepareDescriptors
applyDescriptors

exit 0
