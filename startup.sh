#!/bin/bash

# VARIABLES
CPUS=3  # minimum=3, recommended=4
MEMORY=6144    # minimum=6144, recommended=10240
KUBERNETES_VERSION=v1.26.1 

# FUNCTIONS
function kubernetesCheck() {
    echo -e "\n*** Checking Kubernetes Cluster ***"
    if [ $(kubectl config get-contexts --no-headers | wc -l) == 0 ]
    then 
        minikube start --cpus $CPUS --memory $MEMORY --kubernetes-version $KUBERNETES_VERSION 
        minikube addons enable dashboard 
        minikube addons enable metrics-server
        minikube addons enable ingress
        minikube profile list
        minikube addons list
    elif [ $(kubectl config get-contexts --no-headers | wc -l) -gt 1 ]
    then
        echo -e "\nPlease choose one of the following clusters: "
        CURRENT=$(kubectl config current-context)
        kubectl config get-contexts
        read -p "Cluster name? (default: ${CURRENT}) " CONTEXT
        if [[ $CONTEXT != "" ]]
        then
            kubectl config use-context $CONTEXT
        fi
    fi
    kubectl config current-context
    kubectl cluster-info
}

# STEPS
kubernetesCheck
