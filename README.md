<div align="center">
             <img src="logo.png" alt="GitLab Logo" width="256" />
             <h1>Using GitLab with a Kubernetes cluster</h1>
</div>

Our project's main goal is to allow you to install a GitLab instance, and to register a GitLab-Runner or GitLab-Agent to your Kubernetes cluster through the use of documented convenience scripts.

# Prerequisites

## System Requirements

**OS**: Debian/Ubuntu 

**[minikube in docker](https://minikube.sigs.k8s.io/docs/drivers/docker/)**

## Recommended 

For a quick head start, we recommend using the [Linux Config Bootstrap](https://gitlab.com/devops-collab/linux-config-bootstrap) repository. This will allow you to have a **new clean instance** of your virtual machine up and running with the correct configuration.

# Getting Started

## Linux Terminal

Open a terminal and clone the project to your `$HOME` folder:

```bash
cd $HOME && git clone https://gitlab.com/devops-collab/gitlab-kube.git
```

Navigate to the root of the repository:

```bash
cd ~/gitlab-kube/
```

# Registering a _[GitLab-Agent](https://docs.gitlab.com/ee/user/clusters/agent/install/#install-the-agent-in-the-cluster)_

Launch `agent-register.sh` appending your GitLab Runner Token and follow instructions:

```bash
./agent-register.sh  #TOKEN
```

**NB**: When prompted to enter a name for your GitLab Agent, you may ONLY use lowercase alphanumeric values and the character `-` (for example: `gitlab-agent-012345`)

# Registering a _[GitLab-Runner](https://docs.gitlab.com/runner/install/kubernetes.html)_

Launch `runner-register.sh` appending your GitLab Runner Token and follow instructions:

```bash
./runner-register.sh  #TOKEN
```

**NB**: 

- When prompted to enter your GitLab URL, please be sure to end it with a `/` character (for example: `https://gitlab.127.0.0.1.nip.io/`)

- When prompted to enter a name for your GitLab Runner, you may ONLY use lowercase alphanumeric values and the character `-` (for example: `gitlab-runner-012345`)

# Installing a _[GitLab Instance](https://docs.gitlab.com/charts/development/minikube/)_


Launch `gitlab-setup.sh` and follow instructions:

```bash
./gitlab-setup.sh
```

**NB**: This may take up to 20 minutes.

Monitor your deployment using minikube dashboard:

```bash
minikube dashboard --url
```

Open a tunnel to make your GitLab instance accessible from your web browser:

```bash
minikube tunnel
```


## File Explorer

Add the domain name to your **/etc/hosts** file (elevated privileges may be required):

```bash
127.0.0.1 127.0.0.1.nip.io
```

**NB**: path to **/etc/hosts** may vary depending on your OS (on Windows 10 for example: `C:\Windows\System32\drivers\etc\hosts`)



## Web browser

Access your GitLab instance:

```bash
https://gitlab.127.0.0.1.nip.io/
```

**NB**: You may wish to add the generated `gitlab.127.0.0.1.nip.io.ca.pem` certificate to your Web browser to establish a secure connection.

## You are all set!

Enjoy using GitLab with a Kubernetes cluster. Spread the word, give this project a star, and feel free to reach out for suggestions.

# Credits

- [Thibault Charrin](https://gitlab.com/thibaultcharrin) (_DevOps | Full Stack Developper_)
    
    - Main author, script implementation

- [NeXT](https://macosicons.com/#/u/NeXT)

    - Logo


