#!/bin/bash

# Resources
# https://docs.gitlab.com/runner/install/kubernetes.html
# https://gitlab.com/gitlab-org/charts/gitlab-runner/-/blob/d623d12308676568f8993526eccf493078d43421/values.yaml
# https://docs.gitlab.com/runner/executors/kubernetes.html#configuring-executor-service-account

# The following settings were changed to allow GitLab CI/CD pipeline to work correctly
    # rbac: create: true
    # clusterWideAccess: true
    # serviceAccountName: default

# VARIABLES
TOKEN=""
GITLAB_URL=https://gitlab.com/
GITLAB_RUNNER=gitlab-runner-$(echo $1 | cut -c 1-13 | tr '[:upper:]' '[:lower:]' | sed "s/[^[:alnum:]-]//g")

# ARGS
if [[ $1 != "" ]]
then
    TOKEN=$1
fi

# FUNCTIONS
function checkToken() {
    echo -e "\n*** Token Check ***"
    if [[ $TOKEN == "" ]]
    then 
        echo -e "\nPlease re-run the script passing the GitLab Runner Token as argument "
        exit 1
    else
        echo "ok"
    fi
}

function kubeCheck() {
    . ./startup.sh
}

function prepareDescriptors() {
    echo -e "\n*** Preparing Descriptors ***"
    read -p "Please enter your GitLab URL (default: https://gitlab.com/) " URL
    read -p "Please enter a name for your GitLab Runner (default: gitlab-runner-<token>) " NAME
    GITLAB_URL=${URL:=$GITLAB_URL}
    GITLAB_RUNNER=${NAME:=$GITLAB_RUNNER}
    echo -e \
    "GITLAB_URL \t: ${GITLAB_URL} \nTOKEN \t \t: ${TOKEN} \nGITLAB_RUNNER \t: ${GITLAB_RUNNER} "
    cp values-template.yaml values.yaml
    sed -i "s|\${GITLAB_URL}|$GITLAB_URL|g" values.yaml
    sed -i "s|\${TOKEN}|$TOKEN|g" values.yaml
}

function applyDescriptors() {
    echo -e "\n*** Applying Descriptors ***"
    helm repo add gitlab https://charts.gitlab.io
    helm repo update
    helm upgrade --install $GITLAB_RUNNER -f values.yaml gitlab/gitlab-runner \
    --namespace gitlab-runner \
    --create-namespace
    
    rm values.yaml
}

# STEPS
checkToken
kubeCheck
prepareDescriptors
applyDescriptors

exit 0
